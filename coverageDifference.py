# -*- coding: utf-8 -*-
import sys,os
import pysam
import HTSeq
import numpy as np
	
#input files, bam_dir
print("reading input...\n")
try:
	gtf = HTSeq.GFF_Reader(sys.argv[1])
	bam_dir = sys.argv[2]
	outfile = open(sys.argv[3], "w")
except(IOException):
	print("Error while reading files")

#get all bamfiles from directory, store all reads from sample in list
print("getting bam files...\n")
samples = []
for paths, dirs, files in os.walk(bam_dir):
	for f in files:
		if f[-4:] == ".bam":
			samples.append(os.path.abspath(f))  
			
#parse gtf
print("parsing annotation file...\n")
genes = {}
for feature in gtf:
	if feature.type == "gene":
		chrom = feature.iv.chrom
		start = int(str(feature.iv).split(":")[1].split(",")[0].split("[")[1])
               	end = int(str(feature.iv).split(":")[1].split(",")[1].split(")")[0])
		name = feature.name
		genes[name] = [chrom,start,end]

#get per base converage, dict perBaseCoverage: {geneName : samples}, where
#sample = [] with coverages per base. len(sample) == #bases
print("computing per base coverage for gene in all samples...\n")
completed = 0
perBaseCoverage = {}
for gene in genes:
	perBaseCoverage[gene] = []
	chrom = genes[gene][0]
	start = genes[gene][1]
	end = genes[gene][2]
	for sample in samples:
		path = bam_dir + "/" + os.path.basename(sample)
		currentSample = pysam.AlignmentFile(path, "rb")
		coverages = []
		nextCol = start
		for pileupcolumn in currentSample.pileup(chrom, int(start), int(end)):
			if pileupcolumn.pos < start or pileupcolumn.pos > end:
				continue
			while nextCol < pileupcolumn.pos:
				coverages.append(0)
				nextCol += 1
			coverages.append(pileupcolumn.n)
			nextCol += 1
		while nextCol <= end:
			coverages.append(0)
			nextCol += 1
		perBaseCoverage[gene].append(coverages)
		currentSample.close()
	completed += 1
	print("completed " + str(completed) + "/" + str(len(genes.keys())))


#compute score, store in scores{gene: [list of scores for each sample]}
print("Calculating scores")
scores = {}
for gene in perBaseCoverage:
	gene_scores = []
	for sample in range(0, len(perBaseCoverage[gene])):
		average_other_samples = [0]*len(perBaseCoverage[gene][0])
		for otherSample in range(0, len(perBaseCoverage[gene])):		
			if sample != otherSample:
				for base in range(0, len(perBaseCoverage[gene][otherSample])):
					average_other_samples[base] += perBaseCoverage[gene][otherSample][base]
		sample_vector = np.array(perBaseCoverage[gene][sample], dtype='f')
		average_vector = np.array(average_other_samples, dtype='f')
		norm_sample_vector = sample_vector/len(perBaseCoverage[gene][sample])
		norm_average_vector = average_vector/len(perBaseCoverage[gene][sample])
		norm_average_vector = norm_average_vector/len(perBaseCoverage[gene])
		dist = np.linalg.norm(norm_sample_vector-norm_average_vector)
		gene_scores.append(dist)
	scores[gene] = gene_scores
		
				
#write output
print("writing output")
header = "gene"
for bamfile in samples:
	header += "\t" + os.path.basename(bamfile)[:-4]
outfile.write(header + "\n")
for gene in perBaseCoverage:
	line = gene
	for value in range(0, len(scores[gene])):
		line += "\t" + str(scores[gene][value])
	line += "\n"
	outfile.write(line)
outfile.close()
