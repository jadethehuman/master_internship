## This script takes the output of meap as input and writes only the most variable transcripts or genes
# to an output file. In this script we compute the coefficient of variation


# -*- coding: utf-8 -*-
import math
import sys, os
from operator import itemgetter

# function to calculate the mean of some values
def mean(lis):
	sum = 0;
	count = 0;
	for i in lis:
		# some transcripts have no expression values, in that case we use "0"
		if(i == "NA\n") or i == "\n":
			i = '0'
		sum = sum + float(i);
		count = count + 1;
	return float(sum) / count;

# function to compute the standart deviation of some values
def sd(lis):
	tmp = 0;
	count = 0;
	m = mean(lis);
	if m < 0.00000000000001 or m > 10:
		return None
	for i in lis:
		# same here, some transcripts have no expression, use 0 instead
		if(i == "NA\n") or i == "\n":
			i = '0'
		sum = pow(float(i)-m, 2)
		tmp = tmp + sum
		count = count +1;	
	return math.sqrt(float(tmp) / (count -1));


# function to calculate the coefficient of variation of some expression values
def cv(lis):
	if sd(lis) != None:
		return sd(lis)/max(mean(lis), 1)
	else:
		return None

# function to read a string and cast to array
def from_line_to_array(str):
	array = []
	line = str.split("\t")
	for i in line[1:]:
		if(i == "NA"):
			i = '0'	
		array.append(i);
	return array

# function checks if the output of baseline method is not 0 for all samples
def check_for_zeros(array):
	boolean = 0
	for i in range(0, len(array)):
		if array[i] != "\n":
			if(float(array[i]) > 0):
				boolean = 1
	return boolean 

#check if min 20% of all samples have a value for a transcript
def check_for_20(lis):
	count = 0
	for i in range(0, len(lis)):
		if lis[i] != "\n":
			if float(lis[i]) != 0.0:
				count += 1
	perc = float(count)/(len(lis)-1)
	if perc >= 0.2:
		return 1
	else: 
		return 0

if len(sys.argv) == 4:
	#input file (transcripts or genes), which is the output of meap or mmbgx
	infile = sys.argv[1]
	# output file, actually the same but only with the most variable transcripts or genes
	outfile = sys.argv[3]
	# the cv cutoff, for the best 60% use 0.6
	cutoff = sys.argv[2]

	f = open(infile)
	# the output will be stored in meap_clustering directory
	newfile = open(outfile, 'w')
	newfile.write(f.readline())

	# store the cv value of an transcript in cv_map with its array index
	cv_map = {}
	zeilenindex = 1
	for l in f.readlines():
		zeilenindex += 1
		key = (l.split("\t")[0])
		ary = from_line_to_array(l)
		if check_for_zeros(ary) != 0 and check_for_20(ary) == 1:
			if sd(ary) != None:
				value = [cv(ary), zeilenindex]
				cv_map[key]=value
			else:
				pass
		else:
			pass

	# sort all cv values to get the best or most variable ones
	bla = sorted(cv_map.values(), key=itemgetter(0))

	# compute how many transcripts/genes we will have with the defined cutoff
	bla.reverse()
	genes = int(len(bla)*float(cutoff))
	best =  bla[0:genes]

	# drop all transcripts
	listofbest= []
	for elem in best:
		listofbest.append(elem[1])

	f.close()
	f = open(infile)

	# write the transcripts/genes with the greatest cv to the output file
	f.readline()
	zeilenindex = 1
	for l in f.readlines():

		zeilenindex += 1
		if zeilenindex in listofbest:
			newfile.write(l)
	f.close()
	newfile.close()
else:
	print "cv only considers events/transcripts/genes, that have scores in at least 20% of all samples."
	print "also this tool calculates cv with sd/max(mean,1), not only sd/max"
	print "usage: "
	print "python cv.py <input> <cv-cutoff in per cent>** <output>"
	print "** for 20 % => 0.2"
