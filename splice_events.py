# -*- coding: utf-8 -*-
# This Python script gets k bam files as input and as output it produces a table for all genes for all splice events with a score
import sys, os 
import pysam as py
import HTSeq as ht
import itertools

# a read is a list wich looks like this: read = [[(start, end), (start, end)], [(start, end), (start, end)]]
# all_exons is a list which looks like this all_exons = [(exonstart, exonend), (exonstart, exonend), (exonstart, exonend)]

def check2(read, all_exons):
	count = 0
	s=''
	for i in range(0,len(read)):
	    for j in range(0, len(all_exons)):
		if i == 0 and read[i][1] == all_exons[j][1]:
		      count += 1
		      s+=str(all_exons[j])
		      break
		elif read[i][0] == all_exons[j][0] and i == len(read)-1:
		      count += 1
		      s+=str(all_exons[j])
		elif read[i][0] == all_exons[j][0] and read[i][1] == all_exons[j][1]:
		      count += 1
		      s+=str(all_exons[j])
		      break
	if count != len(read):
	    return [0]
	else:
	    return[1,s]
	  

gene_list = ["ABCA2","ABCF2","ABCG1","ABI2","AC004967.7","AC009299.3","AC013272.3","AC027612.6","AC092135.1","AC107983.3","AC136007.2","ACACA","ACACB","ACAD9","ACCS","ACOX3","ACTR5","ADARB1","ADCK3","ADCK4","ADIPOR2","AFAP1L2","AFF3","AGPAT3","AGPHD1","AKT1S1","ALAD","ALG13","ALPK1","ANAPC4","ANK3","ANKMY1","ANKS1A","ANXA9","AP3B1","APC","APPBP2","AQP7P3","ARFGAP2","ARHGAP17","ARHGEF16","ARHGEF18","ARL6IP1","ASAP3","ASNS","ASXL1","ATG2A","ATP11B","ATP2C2","ATP7A","ATP9B","ATPBD4","ATXN1","AURKB","B9D1","BAI1","BANF1","BCAS3","BCKDK","BEX5","BICC1","BMP1","BMP5","BOC","BOD1L","BRE","BRF1","BTAF1","BX571672.1","C10orf137","C11orf30","C15orf23","C17orf108","C17orf86","C18orf34","C18orf8","C19orf22","C1orf21","C1orf85","C22orf25","C6orf70","C9orf114","C9orf9","C9orf95","CA4","CA5B","CAPN11","CAPN13","CAPN9","CASP10","CBLC","CCHCread_block","CCNK","CDC42BPB","CDCA2","CDK10","CEACAM19","CELF2","CEP104","CEP164","CEP19","CEP78","CGN","CHD8","CHERP","CHST7","CLASP2","CLIP3","CLN3","CLPX","CNOT2","CNOT6","CNTFR","COBL","COL9A3","COMMD1","CORO7","CPAMD8","CPSF3","CPSF7","CR392039.1","CRLF3","CROCC","CRTC2","CSDA","CSNK1G1","CSPP1","CSRP2BP","CTBP2","CTC-358I24.1","CTDSPL2","CTSG","CTTNBP2NL","CYB5R4","CYP2read_block","DCAF5","DDI2","DDX60L","DEDD","DEF6","DGKD","DGUOK","DHX9","DICEread_block","DIMT1L","DIP2A","DLG5","DNAJC13","DNAJC4","DOC2B","DOCK10","DOCK11","DOCK6","DOCK8","DOCK9","DPAGT1","DPP4","DTNA","DUS1L","DYRK1A","EEF1D","EFCAB7","EGFR","EHBP1","EHMT2","EIF2C3","EIF2C4","ELMO2","ELN","EMBP1","EMID1","ENG","ENO1","EPB41L4B","EPHX1","EPN2","ERI2","EVI5","EXOC2","EXOSC10","F10","F2RL3","FAHD2A","FAM102A","FAM116A","FAM116B","FAM117B","FAM160A1","FAM185BP","FAM38A","FAM48A","FAM65B","FGD3","FGFread_blockOP","FHL3","FLCN","FLNC","FMN1","FMNL2","FNTA","FOXJ2","FOXP1","FRA10AC1","FRYL","FTO","FUT8","FYN","GALNT4","GCDH","GCLC","GLE1","GLYCTK","GLYread_block","GNAI1","GPATCH8","GPread_block24","GPR98","GRAMD4","GRIPAP1","HAGH","HAS1","HAUS8","HCFC2","HCG15","HDAC5","HEATR5A","HELZ","HERPUD2","HEXDC","HGSNAT","HMGN1","HOXC10","HPS1","HPS5","HSCB","HSDL2","HSPA8","IFRD2","IFT81","IGF1R","IGF2BP2","IGSF9","IKBKB","IL32","INADL","INPP4B","INTS2","INTS3","IQCA1","IRAK4","ITCH","ITGB6","ITPR3","ITSN2","JAG1","JMJD7-PLA2G4B","JPX","KCNQ5","KDM4A","KIAA0467","KIAA0753","KIAA1217","KIAA1274","KIAA1429","KIAA1797","KIF1B","KIF3A","KIT","KLHL22","KRI1","KYNU","LAD1","LAMA2","LARGE","LARP1B","LARS","LCORL","LDB2","LDLRAP1","LEF1","LGALS7B","LGR4","LHPP","LIMK1","LIMK2","LNX1","LPCAT2","LPCAT4","LPP","LRBA","LRRC1","LRRC16A","LRRC23","LRRC28","LRRC45","LRRC48","LTN1","LY6K","LY75-CD302","LYPD6B","MACROD1","MACROD2","MADD","MALT1","MAMDC2","MAN2A2","MAP2K2","MAP3K2","MAP4K3","MAPK11","MAPK8IP3","MAPK9","MARK2","MAT2A","MAVS","MBD5","MBOAT1","MCF2L","MCM5","MDN1","ME3","MED14","MED24","MED6","MEF2C","MEF2D","MEIS2","MELK","METTL17","MGA","MGLL","MGRN1","MGST2","MICALL2","MID2","MIPEP","MKRN1","MLH1","MLKL","MLL2","MLL5","MLXIP","MMRN1","MPND","MPP5","MPRIP","MRE11A","MRPL24","MRPL55","MRPS17","MRS2","MSH5","MSI2","MTF1","MTHFSD","MTL5","MTOR","MYO9A","MYSM1","NBAS","NBEAL1","NBPF3","NCAPD2","NCAPG2","NCBP1","NCOA7","NCOR2","NCRNA00219","NCRNA00277","NDUFS7","NELL2","NF2","NFIA","NFIB","NFXL1","NIN","NISCH","NLK","NOTCH3","NR4A1","NRG2","NRP2","NTRK3","NUBPL","NVL","OCRL","ODF2L","P2RY10","PABPC1L","PACSIN3","PARP16","PARP2","PCGF5","PCP2","PDE1A","PDE4DIP","PDIA3","PDPR","PDZK1","PER3","PEX1","PGBD1","PGK1","PGS1","PHACTR2","PHF12","PHF14","PHF16","PHLDB2","PI4KA","PIK3C2B","PIKFYVE","PIP5K1B","PKP4","PLEKHA6","PLEKHG4","PLK3","PLXNA1","PMM2","POC1B","POLG","POLI","POLR2F","POM121","PON3","POT1","PPFIA3","PPL","PPP1read_block2A","PPP1read_block2B","PPP3CB","PRKDC","PRPSAP2","PRRC2C","PSIP1","PSMD12","PTCD3","PTOV1","PTPN13","PTPRG","PUS7","PYROXD2","RAB3IP","RABGAP1L","RABGGTA","RAP1GAP","RAPGEF6","RASEF","RASSF5","RAVEread_block","RBFOX2","RBM11","RBM19","RBM41","RBM47","RBMS3","RC3H2","REEP4","REPS1","REV3L","RFWD2","RFX1","RGL1","RGS19","RHBDF1","RICTOR","RNF216","RNPC3","ROGDI","RP11-14N7.2","RP11-15K19.2","RP11-189B4.6","RP11-428C6.1","RP11-454C18.2","RP11-762I7.5","RP11-86H7.1","RP4-697K14.7","RPL21","RPLP0","RPS24","RPS5","RPSA","RPTOR","RRAS2","RSAD1","SCAF8","SCAPER","SCUBE2","SDCCAG8","SEC16B","SEMA3B","SEMA3D","SEMA3E","SEMA6C","SENP3","SETD4","SFI1","SFPQ","SGIP1","SGSM2","SH3BP2","SH3D21","SHROOM1","SLC25A38","SLC35E2","SLC35E2B","SLC35F2","SLC36A4","SLC4A3","SLC6A10P","SLC6A9","SLC9A1","SLTM","SMG5","SMURF2","SMYD1","SNHG1","SNORD14C","SNRPN","SNX29","SORCS2","SORL1","SP110","SPAST","SPATA6","SPEF2","SPHK2","SPOPL","STAB1","STAC","STK10","STK11IP","STK39","STUB1","STXBP3","SUSD4","SUV39H2","SUZ12P","SYMPK","SYNE1","SYNE2","SYTL1","TAF1A","TAF1C","TAF1D","TAF2","TANC1","TANC2","TANK","TARS2","TBC1D22A","TBC1D5","TBCD","TCEA2","TCEAL7","TCEB1","TCERG1","TCF12","TCOF1","TCP1","TCP11L2","TCTN2","TDRD3","TEAD1","TESC","TFAP2A","TFPI","THOC2","TIAM1","TIAM2","TJP1","TLK1","TM7SF2","TMEM104","TMEM117","TMEM126B","TMEM173","TMEM191A","TMEM45B","TMEM63A","TMEM71","TNFSF12-TNFSF13","TNFSF13","TNRC18","TPD52L1","TPI1","TRAF5","TRAPPC10","TREX2","TRIM24","TRMT1","TRPM4","TSC1","TSread_block","TTC21B","TTC27","TTLL12","TYK2","UBE2D1","UBE2E2","UBE4B","UBN1","UBN2","UCHL3","UEVLD","UHRF2","ULBP2","ULK4","UNC119","UPF3B","USP14","USP28","USP3","USP32","USP4","USP48","UTP20","VPS13A","VPS13D","WDR5","WDR7","WIPI2","WNK2","WRAP73","WWC2","YAP1","ZBTB34","ZBTB41","ZBTB48","ZBTB8OS","ZC3H13","ZC3H7A","ZCCHC2","ZDHHC13","ZEread_block","ZFAND2B","ZFYVE26","ZMYM6","ZNF431","ZNF45","ZNF528","ZNF544","ZNF562","ZNF609","ZNF714","ZNF83","ZP3","ZSWIM7","ZW10"]



# reading in the gtf file
gtf = ht.GFF_Reader(sys.argv[2])
gtf_genes = {}
for feature in gtf:
    if (feature.type == "exon"):
	name = str(feature.attr["gene_name"])
	trans_id = str(feature.attr["transcript_id"])
	start = int(str(feature.iv).split(":")[1].split(",")[0].split("[")[1])
	end = int(str(feature.iv).split(":")[1].split(",")[1].split(")")[0])
	if name in gene_list:
	    if name not in gtf_genes.keys():
		gtf_genes.setdefault(name, {})[trans_id] = [(start, end)]
	    else:
		if trans_id in gtf_genes[name].keys():
		    gtf_genes[name][trans_id].append((start, end))
		else:
		    gtf_genes[name][trans_id] = [(start, end)]

		    

# reading in bam file
out = {}

def execution(bam, c, c2):
    bam_file = py.AlignmentFile(bam, "rb")
    for feature in gtf:   
	if feature.type == "gene":
	    start = int(str(feature.iv).split(":")[1].split(",")[0].split("[")[1])
	    end = int(str(feature.iv).split(":")[1].split(",")[1].split(")")[0])
	    name = str(feature.attr["gene_name"])
	    for read in bam_file.fetch(feature.iv.chrom, start, end):
		if name in gene_list:
		    for trans_id in gtf_genes[name].keys():
			if len(read.get_blocks()) > 1:
			#if check1(read_dict[read], gtf_genes[name][trans_id]):
			    #print "check1"
			  
			    res = check2(read.get_blocks(), gtf_genes[name][trans_id])
			    if res[0]:
				if res[1] in out:
				    out[res[1]][c2] += 1
				else:
				    out[res[1]] = list(itertools.repeat(0,c))
				    out[res[1]][c2] = 1		   
	






if len(sys.argv) == 4:
    path = sys.argv[1]
    bam_file_dir = os.path.dirname(path)
    outfile = open(sys.argv[3] ,'wa')
    outfile.write(str("splice_event"+"\t"))
  
    count = 0
      
    for paths,dirs, files in os.walk(bam_file_dir):
      for f in range(0, len(files)):
	if os.path.splitext(files[f])[1] == '.bam':
		count +=1
		outfile.write(str(files[f]+"\t"))

    outfile.write("\n")
    counti = 0
    for paths,dirs, files in os.walk(bam_file_dir):
	for f in range(0, len(files)):
	  if os.path.splitext(files[f])[1] == '.bam':
		execution(path+files[f], count, counti)
		counti+=1

    for event in out:
	outfile.write(str(event+"\t"))
	for i in range(0,count):
	    outfile.write(str(out[event][i])+"\t")

	outfile.write("\n")
else:
    print "Usage:"
    print "splice_events.py <path_to_bamfiles> <path_to_gtf_file> 'output_name'"



