# -*- coding: utf-8 -*-
import sys, os
import numpy as np

from sklearn import datasets
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.externals import joblib

def usage():
    sys.exit("Usage: python multiclass.py --input <feature table> --label_file <labels for training samples> --train <list of training samples> --predict <list of samples to predict> --model <abs path to modelfile> --out <prediction outputfile> \n \
		--input: absolute path to tab separated feature table with header \n \
		--label_file: tab separated file with sample names in first column and labels in the second \n \
		--train: comma sepeated list of training sample names \n \
		--predict: comma sepeated list of samples to predict, no whitespaces allowd! \n \
		--model: absolute path to model file in .pkl format \n \
		--out: absolute path to output file, is generated in cwd otherwise \n \
		Either --train or --predict must be specified. --train requires a prefix, --predict requires the model to be loaded" )
		
#get cmdline  arguments
cmdline = sys.argv
if ("--input" in cmdline and "--label_file" in cmdline) and (("--train" in cmdline and "--train_model_prefix" in cmdline) or ("--predict" in cmdline and "--out" in cmdline and "--model" in cmdline)):
	if "--input" in cmdline:
		input_index = cmdline.index("--input")
		featuretable = cmdline[input_index + 1]
	if "--label_file" in cmdline:
		label_index = cmdline.index("--label_file")
		labelfile = cmdline[label_index + 1]
	if "--train" in cmdline and "--model" in cmdline:
		train_index = cmdline.index("--train")
		trainsamples = cmdline[train_index + 1]
		model_index = cmdline.index("--model")
		path_to_model = cmdline[trainprefix_index + 1] 
	if "--predict" in cmdline and "--out" in cmdline and "--model" in cmdline:
		model_index = cmdline.index("--model")
		classifier_model = cmdline[model_index + 1]
		predict_index = cmdline.index("--predict")
		predictsamples = cmdline[predict_index + 1]
		out_index = cmdline.index("--out")
		outfile = cmdline[out_index + 1]
else:
	usage()
		

#get feature vectors
print "getting features"
features = {}
try:
	feature_vector_table = open(featuretable, "r") 
	line = feature_vector_table.readline()
	line = line[:-1]
	header = line.split("\t")
	for sample in range(1, len(header)):
		features[header[sample]] = []
	while line != "":
		line = feature_vector_table.readline()
		fields = line.split("\t")[:-1]
		for field in range(1, len(fields)):
			s = header[field]
			features[s].append(float(fields[field]))
except:
	usage()


#construct vectors
training_X = []
predict_X = []
print "construct feature vectors"
if "--train" in cmdline:
	for feature in trainsamples.split(","):
	    if feature.startswith("SRR") and (feature in features.keys()):
		training_X.append(features[feature])
if "--predict" in cmdline:
	for feature in predictsamples.split(","):
	    if feature.startswith("SRR") and (feature in features.keys()):
		predict_X.append(features[feature])

#get labels
if "--train" in cmdline:
	labels = {}
	training_y = []
	print "get lables from file"
	try:
	    label_table = open(labelfile, "r")
	    header = label_table.readline()
	    line = label_table.readline()
	    while line != "":
		key = line.split("\t")[0]
		value = line.split("\t")[1][:-1]
		labels[key] = value
		if key in trainsamples:
		    training_y.append(value)
		line = label_table.readline()
	    #train
	    print "training model ..."
	    model = OneVsRestClassifier(LinearSVC(random_state=0)).fit(training_X, training_y)
	    joblib.dump(model + '.pkl') 
	except:
	    print "getting labels failed"
	    usage()

	

#predict
if "--predict" in cmdline:
	print "predicting"
	model = joblib.load(classifier_model) 
	o = open(outfile, "w")
	scores = model.decision_function(predict_X)
	predict_y = model.predict(predict_X)
	o.write("Samplename" + "\t" + "score" + "\t" + "label" + "\n")
	for i in range(0,len(scores)):
		o.write(predictsamples.split(",")[i] + "\t")
		o.write(str(scores[i])[1:-1] + "\t")
		o.write(str(predict_y[i]) + "\n")
	o.close()
