import sys, os
import subprocess
import itertools
import shutil
from glob import glob

input_data = {}
no_of_samples = 0

for i in range(0,len(sys.argv)):
	if sys.argv[i] == "-in":
		j = i+1
		while sys.argv[j][0] != "-":
			no_of_samples += 1
			input_data[no_of_samples] = [(sys.argv[j] + "fw.fastq")]
			input_data[no_of_samples].append(sys.argv[j] + "rw.fastq")
			j += 1
	elif sys.argv[i] == "-out":
		out = open(sys.argv[i+1], 'w')
	elif sys.argv[i] == "-fasta":
		fasta = sys.argv[i+1]
	elif sys.argv[i] == "--help":
		sys.exit("Usage: \n python rsem.py <options>\n options:\n -in <path_to_fastq's>(must be in separate folder)\n -fasta <path_to_transcriptome_fasta>\n -out <path_to_outfile>")

#prepare-reference
subprocess.call(["/home/proj/biocluster/praktikum/neap15/software/rsem/rsem-1.2.20/rsem-prepare-reference", "--bowtie2", "--bowtie2-path", "/home/proj/biocluster/praktikum/neap15/software/bowtie", fasta, "homo_37_75"])

#calculate-expression
results = {}
count = 0
for samples in input_data:
	print "Sample: ", input_data[samples][0], input_data[samples][1] 
	subprocess.call(["/home/proj/biocluster/praktikum/neap15/software/rsem/rsem-1.2.20/rsem-calculate-expression", "--bowtie2", "--bowtie2-path", "/home/proj/biocluster/praktikum/neap15/software/bowtie", "-p", "8" ,"--paired-end", input_data[samples][0], input_data[samples][1] ,"homo_37_75", "rsem" + str(count)])
	f = open("rsem" + str(count) + ".isoforms.results", "r")
	f.next()
	for line in f:
		l = line.split(" ")[0].split("\t")
		if l[0] not in results:
			results[l[0]] = list(itertools.repeat(0, no_of_samples))
		if l[len(l)-2] != "":
			results[l[0]][count] =  l[len(l)-2]
		elif l[len(l)-3] != "":
			results[l[0]][count] =  l[len(l)-3]
		elif l[len(l)-4] != "":
			results[l[0]][count] =  l[len(l)-4]
		elif l[len(l)-5] != "":
			results[l[0]][count] =  l[len(l)-5]
	f.close()
	print "Glob", glob("rsem" + str(count) + "*")
	for item in glob("rsem" + str(count) + "*"):
		if item == "rsem" + str(count)+".stat" or item == "rsem" + str(count)+".temp":
			shutil.rmtree(item, ignore_errors=True)
		else:
			os.remove(item)
	count += 1
	
# delete all cheese
for item in glob('homo_37_75*'):
		os.remove(item)

#write output
out.write("TranscriptID" + "\t")
for ids in input_data.keys():
	out.write(str(ids) + "\t")
out.write("\n")	

for trans in results:
	out.write(trans + "\t")
	for i in range(0, len(results[trans])):
		out.write(results[trans][i] + "\t")
	out.write("\n")
out.close()