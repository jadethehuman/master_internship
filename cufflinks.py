import sys, os
import subprocess
import shutil

def execute(input_data, gtf, outfile):
	print "starting execution"
	iso_dict = {}
	for sam in input_data:
		sam_split = sam.split("/")
		print "starting cufflinks on", sam_split[len(sam_split)-1]  
		sample = sam_split[len(sam_split)-1].split(".")[0]
		all_files.append(sample)
		subprocess.call(["mkdir", "-p" , sample])	
		subprocess.call(["/home/proj/biocluster/praktikum/neap15/software/cufflinks-2.2.1.Linux_x86_64/cufflinks", "-o", sample, "-p 8", "--library-type", "fr-firststrand",  "-G", gtf, sam])
		iso = open(sample + "/isoforms.fpkm_tracking", "rb")
		iso.next()
		for line in iso:
			trans = line.split("-")[0]
			if trans not in iso_dict:
				iso_dict[trans] = []
			iso_dict[trans].append(line.split(" ")[0].split("\t")[9])
		print sam_split[len(sam_split)-1], " ......  DONE!" 

	out = open(outfile, "wb")
	#create header
	out.write("Transcript_ID" +"\t")
	for entry in input_data:
		#print entry
		out.write(entry.split("/")[len(entry.split("/"))-1] + "\t")
	out.write("\n")

	# iso_dict = {"ENST1" : [0.01, 0.001], "ENST2": [0.01, 0.9], ...}
	print "write output file", sys.argv[3]
	for entry in iso_dict.keys():
		trans_id = str(entry.split("\t")[0])
		out.write(trans_id + "\t")
		for fpkm in iso_dict[entry]:
			out.write(str(fpkm) + "\t")
		out.write("\n")
	iso.close()
	out.close()

	for sample in all_files:
		shutil.rmtree(sample, ignore_errors=True)
	return 1

try:
	all_files = []
	input_data = []
	how_to_start = ""

	for i in range(0,len(sys.argv)):
		if sys.argv[i] == "-in":
			j = i+1
			while sys.argv[j][0] != "-":
				input_data.append(sys.argv[j])
				j += 1
		elif sys.argv[i] == "-out":
			outfile = sys.argv[i+1]
		elif sys.argv[i] == "-gtf":
			gtf = sys.argv[i+1]
		elif sys.argv[i] == "--help":
			sys.exit("Usage: \n python cufflinks.py <options>\n options:\n -in <path_to_bams>\n -gtf <path_to_gtf>\n -out <path_to_outfile>")

	execute(input_data, gtf, outfile)
except:
	sys.exit("Usage: \n python cufflinks.py <options>\n options:\n -in <path_to_bams>\n -gtf <path_to_gtf>\n -out <path_to_outfile>")


#print "apply cufflinks output on cv....."
#subprocess.call(["python", "/home/proj/biocluster/praktikum/neap15/results/Simona/baselinemethods/cv.py", sys.argv[3], "0.6", "/home/proj/biocluster/praktikum/neap15/results/Simona/cufflinks_2_cv.txt"])

#svm!!!
